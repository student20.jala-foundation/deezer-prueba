export interface canciones{
  artist : string,
  img ?: string,
  name : string

}

export interface InformationCard extends canciones{
  album : string,
  releaseYear : number,
  playTime: string,
  trackNumber: number
}

export interface Album {
  cover: string,
  cover_big ?: string,
  cover_small ?: string,
  id : string,
  title : string,
  tracklist : string,
  type : string
}

export interface Artist{
  id: number,
  link : string,
  name : string,
  radio ?: boolean,
  tracklist : string,
  type : string

}

export interface SongInformation{
  id : number,
  album : Album,
  artist : Artist,
  duration : number,
  link : string,
  title : string


}



