import { TestBed } from '@angular/core/testing';

import { InformationAboutSongsService } from './information-about-songs.service';

describe('InformationAboutSongsService', () => {
  let service: InformationAboutSongsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InformationAboutSongsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
