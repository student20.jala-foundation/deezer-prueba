import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SongsService {

  apiUrl = 'https://cors-anywhere.herokuapp.com/https://api.deezer.com/chart/0/tracks';
  constructor(
    private http: HttpClient
    ) { }


    getSongs(){
      return this.http.get(this.apiUrl)
    }
}
