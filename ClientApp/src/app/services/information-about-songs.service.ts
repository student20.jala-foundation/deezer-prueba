import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { SongInformation } from '../model/canciones.model';

@Injectable({
  providedIn: 'root'
})
export class InformationAboutSongsService {

  apiUrl = 'https://cors-anywhere.herokuapp.com/https://api.deezer.com/chart/0/tracks';

  constructor(
    private http: HttpClient
  ) { }


  getSongs(){
    return this.http.get<SongInformation>(this.apiUrl)
  }


}
