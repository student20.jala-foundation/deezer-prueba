import { Component, OnInit } from '@angular/core';

import { InformationCard } from 'src/app/model/canciones.model';

@Component({
  selector: 'app-information-card',
  templateUrl: './information-card.component.html',
  styleUrls: ['./information-card.component.css']
})
export class InformationCardComponent implements OnInit {

  informationCard : InformationCard = {
     artist : 'Gunna',
     name : 'Pushin P (feat. Young Thug)',
     album : 'DS4EVER',
     releaseYear : 2022,
     playTime : '2:16',
     trackNumber : 2,
     img : '../../../assets/img/album.jpg'
  }


  constructor() { }

  ngOnInit(): void {
  }

}
