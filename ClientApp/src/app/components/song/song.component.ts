import { Component, OnInit } from '@angular/core';

import { SongsService } from 'src/app/services/songs.service';

@Component({
  selector: 'app-song',
  templateUrl: './song.component.html',
  styleUrls: ['./song.component.css']
})
export class SongComponent implements OnInit {

  dataApi :any= {};

  constructor(
    private song : SongsService
  ) { }

  ngOnInit(): void {
    this.song.getSongs()
    .subscribe(data => {
      console.log('Conectado a deezer' , data)
      this.dataApi = data;
    })

  }

}
