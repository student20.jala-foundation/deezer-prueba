import { Component, OnInit } from '@angular/core';

import { canciones } from 'src/app/model/canciones.model';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  song : canciones = {
    name : 'bad',
    artist : 'Michael Jackson',
    img : '../../../assets/img/album.jpg'
  }

  iconPlay : string = '../../../assets/img/play.png';

  constructor() { }

  ngOnInit(): void {
  }

}
