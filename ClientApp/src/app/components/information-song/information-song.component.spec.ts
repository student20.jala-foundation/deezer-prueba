import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformationSongComponent } from './information-song.component';

describe('InformationSongComponent', () => {
  let component: InformationSongComponent;
  let fixture: ComponentFixture<InformationSongComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformationSongComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationSongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
