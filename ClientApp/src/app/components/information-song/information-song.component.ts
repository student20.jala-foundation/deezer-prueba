import { Component, OnInit } from '@angular/core';

import { InformationAboutSongsService } from 'src/app/services/information-about-songs.service';

import { SongInformation } from 'src/app/model/canciones.model';
@Component({
  selector: 'app-information-song',
  templateUrl: './information-song.component.html',
  styleUrls: ['./information-song.component.css']
})
export class InformationSongComponent implements OnInit {

  informationSongc : SongInformation = {
    id : 0,
    album : {
      cover: '',
      id : '',
      title : '',
      tracklist : '',
      type : ''
    },
    artist : {
      id: 0,
      link : '',
      name : '',
      tracklist : '',
      type : ''
    },
    duration : 0,
    link : '',
    title : ''

  }


  constructor(
    private informationSong : InformationAboutSongsService

  ) { }

  ngOnInit(): void {
    this.informationSong.getSongs()
    .subscribe(data => {
      console.log('Conectado a deezer' , data)
      this.informationSongc = data;

    })

  }

}
